<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Gis</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background-color: #e0e0e0; background-position: bottom right; background-repeat: no-repeat;">
    <tr>
        <td align="center" valign="top">
            <table cellpadding="0" cellspacing="0" border="0" width="580px" style="font-family: 'Montserrat' , 'Roboto', sans-serif;padding: 64px 0;">
                <tr>
                    <td align="center" valign="top">
                        <a href="https://vegaglobal.org/" target="_blank">
                            <img src="https://vega-core.demoview.info/img/email/black-vega.png" alt="VegaGlobal" width="100">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" height="30px"></td>
                </tr>
                <tr>
                    <td>
                        <table style="box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.1); border-radius: 2px; background: #FFFFFF;" cellspacing="0" cellpadding="0" border="0" width="640px">
                            <tr>
                                <td>
                                    <div style="height: 40px"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="width: 60px"></div>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td style="text-align: left;">
                                                            <h2 style="font-size: 24px; font-weight: 700; margin:5px 0 20px;">Код подтверждения.</h2>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="color: #777777; font-size: 34px; font-weight: 400;">
                                                            <p>{{ $code }}</p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size: 14px; font-weight: 400;">
                                                            <p>Используйте этот проверочный код, чтобы подтвердить свои действия.<br><br>Если отправка этого кода не была инициирована вами, незамедлительно сообщите об этом в службу поддержки.</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <div style="width: 60px"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="background-color: #F8F8F8" width="100%">
                                        <tr>
                                            <td>
                                                <div style="height: 40px"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="60px">
                                                <div style="width: 60px"></div>
                                            </td>
                                            <td>
                                                <h3 style="font-weight: 900; color: #00000; margin: 0 0 10px 0">Copyright © VegaGlobal 2022</h3>
                                                <a href="mailto:info@vegaglobal.org" style="font-weight: 900; color: #878787; text-decoration: none">info@vegaglobal.org</a>
                                            </td>
                                            <td width="60px">
                                                <div style="width: 60px"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="height: 40px"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>

</html>
