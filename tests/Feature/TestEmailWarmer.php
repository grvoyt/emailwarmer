<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Services\EmailWarmer;

class TestEmailWarmer extends TestCase
{
    public function test_warmer()
    {
        $emailwarmer = new EmailWarmer;

        $emails = [
            '9999777db@gmail.com',
            'a@invi.pw',
            'aleksandar.rizhak@yandex.ru',
            'arnautd88@gmail.com',
            'art888kor@gmail.com',
            'b@invi.pw',
            'baril.baril@yandex.ru',
            'bogdanovjo@gmail.com',
            'bogdanovjo@mail.ru',
            'bogdanovjo1994@mail.ru',
            'brerinnda@yandex.ru',
            'c@invi.pw',
            'cocervei.sergiu@gmail.com',
            'cocervei7@gmail.com',
            'cryptozagg@mail.ru',
            'cryptozagg@yandex.ru',
            'cs.spark2012@mail.ru',
            'direvin@mail.ru',
            'dmakhnach@yandex.ru',
            'dmitry.makhnach1996@gmail.com',
        ];

        $emailwarmer->setEmails($emails);

        $emailwarmer->queue();

        self::assertTrue(true);
    }
}
