test_local:
	php artisan migrate:fresh
	php artisan emails:start ./email.txt
	php artisan	jobs:table

install:
	composer i
	cp .env.example .env
	touch database/database.sqlite
	touch email.txt
	php artisan	migrate:fresh --seed

test:
	php artisan email:test

