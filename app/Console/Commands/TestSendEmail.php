<?php

namespace App\Console\Commands;

use App\Mail\NewAuthCode;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class TestSendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправка тестового email';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $meail = $this->ask('Enter email','jobiles@mail.ru');
            Mail::to($meail)
                ->send(new NewAuthCode());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
