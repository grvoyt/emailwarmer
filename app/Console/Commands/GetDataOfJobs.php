<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class GetDataOfJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:table {--limit=100} {--page=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $limit = (int) $this->option('limit') ?? 100;
        $page = (int) $this->option('page');
        $offset = $limit * ($page-1);
        //dd(compact('limit','offset'));
        $jobs = DB::table('jobs')
            ->offset($offset)
            ->limit($limit)
            ->get();
        $headers = [
            'ID','Email','Date'
        ];
        $data = [];

        foreach($jobs as $job) {
            $payload = json_decode($job->payload,true);
            $command = Arr::get($payload,'data.command');
            $obj = unserialize($command);
            $data[] = [
                $job->id,
                $obj->email,
                Carbon::parse($job->available_at)
            ];
        }

        $this->table($headers,$data);

        return 0;
    }
}
