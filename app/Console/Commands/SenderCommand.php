<?php

namespace App\Console\Commands;

use App\Services\EmailWarmer;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SenderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:start {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(EmailWarmer $emailwarmer)
    {
        $path = $this->argument('file');
        $file = file_get_contents($path);

        $emails = Str::of($file)
            ->explode(PHP_EOL)
            ->map(function($email) {
                return trim($email);
            })
            ->filter(fn($email) => !empty($email));

        $this->info('Count emails:'.$emails->count());

        $emailwarmer->setEmails($emails->all())->queue();

        $this->alert('start you queue');

        telegram_send('EMAILWARMER: Закончил добавлять очереди');

        return 0;
    }
}
