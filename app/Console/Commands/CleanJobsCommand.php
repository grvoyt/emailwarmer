<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CleanJobsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:delete {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $path = $this->argument('file');
        $file = file_get_contents($path);

        $emails = Str::of($file)
            ->explode(PHP_EOL)
            ->map(function($email) {
                return trim($email);
            })
            ->filter(fn($email) => !empty($email))
            ->all();

        DB::table('jobs')
            ->orderBy('id')
            ->chunk(1000, function($jobs) use ($emails) {
                $ids = [];
                foreach($jobs as $job) {
                    $payload = json_decode($job->payload,true);
                    $command = Arr::get($payload,'data.command');
                    $l = unserialize($command);
                    if( !in_array($l->email,$emails) ) continue;
                    $ids[] = $job->id;
                }
                DB::table('jobs')->whereIn('id',$ids)->delete();
                $this->line('Line deleted');
            });

        telegram_send('EMAILWARMER: Закончил удалять не существующие');

        return Command::SUCCESS;
    }
}
