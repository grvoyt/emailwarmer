<?php

namespace App\Jobs;

use App\Mail\AuthCodeMail;
use App\Mail\FirstMail;
use App\Mail\NewAuthCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $email)
    {
        //
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $class = Arr::random([
            NewAuthCode::class,
        ]);
        Mail::to($this->email)
            ->send(new $class);
        Log::channel('sended')
            ->debug("{$this->email} - ".now());
    }
}
