<?php

namespace App\Services;

use App\Jobs\SendEmailJob;
use Illuminate\Support\Carbon;

class EmailWarmer
{
    //Сколько в день
    protected array $queue = [
        25,
        50,
        250,
        500,
        1000,
        2000,
        4000
    ];

    protected array $emails;
    private bool $debug;
    protected int $count = 0;

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    public function setEmails(array $emails = []):self
    {
        $this->emails = $emails;
        $this->count  = count($emails);
        return $this;
    }

    public function queue()
    {
        $counts = 24;

        $time_send = null;

        $result = [];

        foreach($this->queue as $emails_hourly) {
            $time_send = is_null($time_send) ? Carbon::now() : $time_send->addDay();
            for($i=0; $i<$counts; $i++) {
                shuffle($this->emails);

                $to_send = $this->makeNewEmails($emails_hourly);

                if( !$this->debug ) {
                    $this->sendEmails($to_send,$time_send);
                }

                if( $this->debug ) {
                    $result[] = [
                        'items' => $to_send,
                        'emails' => collect($to_send)->count(),
                        'sort' => $time_send->format('d-m-Y')
                    ];
                }
            }
        }

        if( $this->debug ) {
            collect($result)
                ->groupBy('sort')
                ->map(function($item) {
                    return $item->sum('emails') / 24;
                })
                ->dd();
        }
    }

    protected function makeNewEmails(int $emails_hourly): array
    {
        $result = [];
        while( count($result) < $emails_hourly ) {
            $length = $emails_hourly - count($result);
            $emails_slised = array_slice($this->emails,0,$length);
            array_push($result,...$emails_slised);
        }

        return $result;
    }

    protected function sendEmails(array $emails,Carbon $time)
    {
        $tt = count($emails)/10;

        $count = ceil($tt);

        $offset = 0;
        for($i=0;$i<10;$i++) {
            $arr = array_slice($emails,$offset,$count);

            $time_send = $time->addMinutes(6);

            foreach($arr as $email) {
                $this->send($email,$time_send);
            }

            $offset += $count;
        }
    }

    protected function send(string $email,Carbon $time)
    {
        SendEmailJob::dispatch($email)->delay($time);
    }
}
