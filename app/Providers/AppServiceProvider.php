<?php

namespace App\Providers;

use App\Services\EmailWarmer;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EmailWarmer::class,function() {
            return new EmailWarmer(config('app.debug'));
        });
    }
}
